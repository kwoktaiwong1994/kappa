$(function () {
    var i, n, o, c;
    i = $("#gray-1"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-2"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-3"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-4"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-5"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-6"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-7"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-8"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-9"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-10"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-11"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-12"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-13"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-14"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-15"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-16"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-17"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-18"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-19"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-20"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-21"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-22"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-23"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-24"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-25"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-26"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-27"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-28"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
}), $(function () {
    var i, n, o, c;
    i = $("#gray-29"), n = document.createElement("canvas"), o = n.getContext("2d"), (c = new Image).onload = function () {
        var a = c.width,
            t = c.height;
        n.width = a, n.height = t, o.drawImage(c, 0, 0);
        for (var e = o.getImageData(0, 0, a, t), d = 0; d < e.height; d++)
            for (var r = 0; r < e.width; r++) {
                var h = 4 * d * e.width + 4 * r,
                    g = (e.data[h] + e.data[1 + h] + e.data[2 + h]) / 3;
                e.data[h] = g, e.data[1 + h] = g, e.data[2 + h] = g
            }
        o.putImageData(e, 0, 0, 0, 0, e.width, e.height), i.attr("src", n.toDataURL())
    }, c.src = i.attr("src")
});